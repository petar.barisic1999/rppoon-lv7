﻿using System;

namespace zadatak1
{
	class Program
	{
		static void Main(string[] args)
		{
			double[] array= new double[] { 2,0.3,7.6,-98.4,7.6,8.42,100,-98.3,5,10};
			NumberSequence numberSequence = new NumberSequence(array);

			BubbleSort bubble = new BubbleSort();
			CombSort comb = new CombSort();
			SequentialSort sequential = new SequentialSort();

			Console.WriteLine( "Original\n"+ numberSequence.ToString());

			numberSequence.SetSortStrategy(bubble);
			numberSequence.Sort();
			Console.WriteLine("Bubble\n" + numberSequence.ToString());

			NumberSequence numberSequence1 = new NumberSequence(array);
			numberSequence1.SetSortStrategy(comb);
			numberSequence1.Sort();
			Console.WriteLine("Comb\n" + numberSequence1.ToString());

			NumberSequence numberSequence2 = new NumberSequence(array);
			numberSequence2.SetSortStrategy(sequential);
			numberSequence2.Sort();
			Console.WriteLine("Sequential\n" + numberSequence2.ToString());


		}
	}
}
