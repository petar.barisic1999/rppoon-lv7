﻿using System;

namespace zadatak3
{
	class Program
	{
		static void Main(string[] args)
		{
			SystemDataProvider systemDataProvider = new SystemDataProvider();
			ConsoleLogger consoleLogger = new ConsoleLogger();
			systemDataProvider.Attach(consoleLogger);
			while (true)
			{
				systemDataProvider.GetCPULoad();
				systemDataProvider.GetAvailableRAM();
				System.Threading.Thread.Sleep(1000);
			}
		}
	}
}
