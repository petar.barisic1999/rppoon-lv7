﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak2
{
	class LinearSearch: SearchStrategy
	{
		public override int Search(double number, double[] array)
		{
			int arraySize = array.Length;
			for (int i = 0; i < arraySize; i++)
			{
				if (array[i] == number)
				{
					return i;
				}
			}
			return -1;

		}
	}
}
