﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak2
{
	abstract class SearchStrategy
	{
		public abstract int Search(double number, double[] array);
	}
}
