﻿using System;

namespace zadatak2
{
	class Program
	{
		static void Main(string[] args)
		{
			double[] array = new double[] { 2, 0.3, 7.6, -98.4, 7.6, 8.42, 100, -98.3, 5, 10 };
			NumberSequence numberSequence = new NumberSequence(array);
			BubbleSort bubble = new BubbleSort();
			numberSequence.SetSortStrategy(bubble);
			numberSequence.Sort();

			LinearSearch linear= new LinearSearch();
			numberSequence.SetSearchStrategy(linear); 
			double number;
			Console.WriteLine("Enter number: ");
			if (double.TryParse(Console.ReadLine(), out number))
			{
				int index = numberSequence.Search(number);
				if (index == -1)
				{
					Console.WriteLine("Number " + number + " is not in array!");
				}
				else
				{
					Console.WriteLine("Index of number "+ number +" is: " + index);
						
				}
			}

		}
	}
}
